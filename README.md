# Clean Insights Infrastructure 📏🏗️💡

## Abstract

Applications using the [Clean Insights](https://cleaninsights.org/) SDKs require
a backend server to report their collected insights to. This repository contains
Ansible-based "infrastructure as code" that can be used to deploy a hardened
server running [Matomo](https://matomo.org/) and the [Clean Insights Matomo
Proxy](https://gitlab.com/cleaninsights/clean-insights-matomo-proxy/). The
Ansible playbook will configure the core services, monitoring (Prometheus and
AlertManager), firewalls, operating system and application hardening and
backups (Tarsnap).

Some files in this repository are encrypted with sops, although the keys are
not encrypted and descriptions can be found in the "Variables" section below.

## Introduction

This repository contains the Ansible playbooks used to deploy Clean Insights
infrastructure:

* Matomo Server:
  * Apache Web Server
  * MySQL
  * [Matomo](https://matomo.org/)
  * [Clean Insights Matomo Proxy](https://gitlab.com/cleaninsights/clean-insights-matomo-proxy/)
  * Prometheus Exporters (node, mysql)
* Monitoring Server:
  * Grafana
  * Prometheus
  * AlertManager
  * Prometheus Exporter (node)

One monitoring host can be used for multiple Matomo instance hosts.

![Deployment Architecture Diagram](https://gitlab.com/cleaninsights/clean-insights-infrastructure/-/raw/master/docs/architecture.png)

